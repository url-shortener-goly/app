package main

import (
	"goly/model"
	"goly/service"
)

func main() {
	model.Setup()
	service.SetupAndListen()
}
